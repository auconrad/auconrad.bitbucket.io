var Proportion__Control_8py =
[
    [ "Motor_P_C", "classProportion__Control_1_1Motor__P__C.html", "classProportion__Control_1_1Motor__P__C" ],
    [ "Delta1", "Proportion__Control_8py.html#a5d7bdd1cd23b57202589c1cc29bff540", null ],
    [ "Duty1", "Proportion__Control_8py.html#a4eb23660472707391848456c4ef8d87b", null ],
    [ "Enc1", "Proportion__Control_8py.html#a825baf074b2052587efb03bbc16f3169", null ],
    [ "Mot1_Cntlr", "Proportion__Control_8py.html#a53b4bf19604df70d62d4fe1389074e69", null ],
    [ "Motor1", "Proportion__Control_8py.html#a14e217593304bcda1abcf67d80d97529", null ],
    [ "pin_EN", "Proportion__Control_8py.html#aa624438f5f5a4f3036d4310a4791f516", null ],
    [ "pin_IN1", "Proportion__Control_8py.html#a0dafcc2016cb51b7a0098deb89b20847", null ],
    [ "pin_IN2", "Proportion__Control_8py.html#a783c2514a12d7afbaba881ffd3e85558", null ],
    [ "pinB6", "Proportion__Control_8py.html#a2c201460e7ac90399fde3e0f0665746f", null ],
    [ "pinB7", "Proportion__Control_8py.html#ad6238b88f209abfd7f53b07c9f3819c5", null ],
    [ "Pos1", "Proportion__Control_8py.html#af5de4f78876c8e7aaf102b7f14e2052f", null ],
    [ "Pos_Old1", "Proportion__Control_8py.html#ac6a31c3f6a869d93835f498d04b72791", null ],
    [ "Set_Pos1", "Proportion__Control_8py.html#a4f8b34756e1342e7cbbf39ef0ba12dff", null ],
    [ "tim4", "Proportion__Control_8py.html#aff3c3648b6c5a4f22741ed9504960782", null ],
    [ "timer", "Proportion__Control_8py.html#a81d1ec1335250994ae76a62fc0379780", null ]
];