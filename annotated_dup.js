var annotated_dup =
[
    [ "Encoder_Code", null, [
      [ "EncoderDriver", "classEncoder__Code_1_1EncoderDriver.html", "classEncoder__Code_1_1EncoderDriver" ]
    ] ],
    [ "IMU_Code", null, [
      [ "IMU", "classIMU__Code_1_1IMU.html", "classIMU__Code_1_1IMU" ]
    ] ],
    [ "Motor_Code", null, [
      [ "MotorDriver", "classMotor__Code_1_1MotorDriver.html", "classMotor__Code_1_1MotorDriver" ]
    ] ],
    [ "Proportion_Control", null, [
      [ "Motor_P_C", "classProportion__Control_1_1Motor__P__C.html", "classProportion__Control_1_1Motor__P__C" ]
    ] ]
];